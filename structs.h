typedef struct{
	char flight_id[6];
	int init;
	int eta;
	int fuel;
	time_t time_s;
}flight_landing;

typedef struct{
	char flight_id[6];
	int init;
	int t_off;
	time_t time_s;
}flight_takeoff;

typedef struct{
	long id;
	int eta;
	int fuel;
}landing_msg;

typedef struct{
	long id;
	int t_off;
}takeoff_msg;

typedef struct{
	long id;
	int slot;
}shm_msg;

