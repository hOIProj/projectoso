//compile as gcc -Wall -pthread -D_REENTRANT gestor_viagens.c -o exe
//não percebo como usar milisegundos com a biblioteca time. fica efectivamente ut = 1000ms até perguntar ao professor


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sys/msg.h>

#include "structs.h"

#define MAX_BUFF 100
//#define DEBUG

//Variables to read from config.txt
int ut;
int T, dt;
int L, dl;
int min, max;
int Tmax, Lmax;
//Variables to read from config.txt

//Thread variables
pthread_t tempi;
pthread_t *t_flights;
pthread_t *l_flights;
int t_index = 0;
int l_index = 0;
//Thread variables

//Synch variables
pthread_mutex_t creation_tmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t creation_lmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t creation_tcond = PTHREAD_COND_INITIALIZER;
pthread_cond_t creation_lcond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t valid_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t valid_cond = PTHREAD_COND_INITIALIZER;
int valid = 0;

pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;
//Synch variables

//Shared memory variables
int tkoff_id;
int *list_takeoff;
int lnd_id;
int *list_land;
//Shared memory variables

//Non shared memory file descriptors
int msgq_id;
FILE* flog;
int fpipe;
//Non shared memory file descriptors

//Time variables and structures
time_t c_time;//exclusively for pretty time formatting when printing to terminal and log.txt
struct tm *f_time;
//Time variables and structures

int tower;//process id


//Redirect signals-----
void sigint(int signum){
	printf("\nShutting down.\n");
	c_time = time(NULL);
	f_time = gmtime(&c_time);
	fprintf(flog, "%2d:%2d:%2d PROGRAM STOPPED\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec);

	#ifdef DEBUG
	printf("Closing stray file descriptors.\n");
	#endif
	fclose(flog);
	close(fpipe);

	#ifdef DEBUG
	printf("Unlinking pipe.\n");
	#endif	
	unlink("INPUT_PIPE");

	#ifdef DEBUG
	printf("Killing control tower process.\n");
	#endif
	kill(tower, SIGKILL);
	wait(NULL);

	#ifdef DEBUG
	printf("Destroying shared memory.\n");
	#endif
	shmctl(tkoff_id, IPC_RMID, NULL);
	shmctl(lnd_id, IPC_RMID, NULL);

	#ifdef DEBUG
	printf("Destroying synch variables.\n");
	#endif
	pthread_mutex_destroy(&creation_tmutex);
	pthread_mutex_destroy(&creation_lmutex);
	pthread_mutex_destroy(&log_mutex);
	pthread_cond_destroy(&creation_tcond);
	pthread_cond_destroy(&creation_lcond);

	#ifdef DEBUG
	printf("Removing message queue.\n");
	#endif
	msgctl(msgq_id, IPC_RMID, NULL);
	exit(0);
}
//Redirect signals-----


//Flight threads-----
void *take_offs(void *t){
	flight_takeoff toff = *((flight_takeoff*)t);
	pthread_cond_broadcast(&creation_tcond);

	c_time = time(NULL);
	f_time = gmtime(&c_time);
	#ifdef DEBUG
	printf("\nThread started at %ld.\n", time(NULL) - toff.time_s);
	#endif
	printf("%2d:%2d:%2d %s was created.\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, toff.flight_id);

	takeoff_msg msg;
	msg.id = 10;
	msg.t_off = toff.t_off;
	msgsnd(msgq_id, &msg, sizeof(msg) - sizeof(long), 0);
	pthread_mutex_lock(&log_mutex);
	fprintf(flog, "%2d:%2d:%2d %s was created.\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, toff.flight_id);
	pthread_mutex_unlock(&log_mutex);
	pthread_exit(NULL);
}

void *landings(void *l){
	flight_landing lnd = *((flight_landing*)l);
	pthread_cond_broadcast(&creation_lcond);

	c_time = time(NULL);
	f_time = gmtime(&c_time);
	#ifdef DEBUG
	printf("\nThread started at %ld.\n", time(NULL) - lnd.time_s);
	#endif
	printf("%2d:%2d:%2d %s was created.\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, lnd.flight_id);

	landing_msg msg;
	msg.id = 20;
	msg.eta = lnd.eta;
	msg.fuel = lnd.fuel;
	msgsnd(msgq_id, &msg, sizeof(msg) - sizeof(long), 0);
	pthread_mutex_lock(&log_mutex);
	fprintf(flog, "%2d:%2d:%2d %s was created.\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, lnd.flight_id);
	pthread_mutex_unlock(&log_mutex);
	pthread_exit(NULL);
}
//Flight threads-----


//Flight creation threads-----
void *l_creation_timer(void *l){
	flight_landing lnd = *((flight_landing*)l);
	valid--;
	pthread_cond_broadcast(&valid_cond);
	long current = time(NULL);

	#ifdef DEBUG
	printf("Waiting for %lds.\n", lnd.init - (current - lnd.time_s));
	#endif
	sleep(lnd.init - (current - lnd.time_s));

	pthread_mutex_lock(&creation_lmutex);
	pthread_create(&l_flights[l_index], NULL, landings, &lnd);
	l_index++;
	pthread_cond_wait(&creation_lcond, &creation_lmutex);
	pthread_mutex_unlock(&creation_lmutex);

	pthread_exit(NULL);
}

void *t_creation_timer(void *t){
	flight_takeoff toff = *((flight_takeoff*)t);
	valid--;
	pthread_cond_broadcast(&valid_cond);
	long current = time(NULL);

	#ifdef DEBUG
	printf("Waiting for %lds.\n", toff.init - (current - toff.time_s));
	#endif
	sleep(toff.init - (current - toff.time_s));

	pthread_mutex_lock(&creation_tmutex);
	pthread_create(&t_flights[t_index], NULL, take_offs, &toff);
	t_index++;
	pthread_cond_wait(&creation_tcond, &creation_tmutex);
	pthread_mutex_unlock(&creation_tmutex);

	pthread_exit(NULL);
}
//Flight creation threads-----


//Tower control process-----
void *t_msgqueue(void *t);
void *l_msgqueue(void *t);
void tower_control();
//Tower control process-----


int main(){
	FILE* f1;
	time_t time_s = time(NULL);//what will be considered the 0th second

	#ifdef DEBUG
	printf("Reading config.txt\n");
	#endif
	f1 = fopen("config.txt", "r");
	if(f1 == NULL){
		perror("Could not open config.txt\n");
		exit(-1);
	}

	#ifdef DEBUG
	printf("Assigning crucial variables.\n");
	#endif
	fscanf(f1, "%d", &ut);
	fscanf(f1, "%d, %d", &T, &dt);
	fscanf(f1, "%d, %d", &L, &dl);
	fscanf(f1, "%d, %d", &min, &max);
	fscanf(f1, "%d, %d", &Tmax, &Lmax);
	fclose(f1);

	#ifdef DEBUG
	printf("Allocating memory for thread arrays.\n");
	#endif
	t_flights = (pthread_t*)malloc(T * sizeof(pthread_t));
	l_flights = (pthread_t*)malloc(L * sizeof(pthread_t));

	/*create int arrays in shared memory. each index of the array is a slot from which flights will read their commands*/
	#ifdef DEBUG
	printf("Creating shared memory.\n");
	#endif
	if((tkoff_id = shmget(IPC_PRIVATE, Tmax * sizeof(int), 0666)) < 1){
		perror("Error allocating shared memory for take offs.\n");
		exit(-1);
	}
	if((lnd_id = shmget(IPC_PRIVATE, Lmax * sizeof(int), 0666)) < 1){
		perror("Error allocating shared memory for landings.\n");
		exit(-1);
	}
	if((list_takeoff = (int*)shmat(tkoff_id, NULL, 0)) < (int*)1){
		perror("Error attaching shared memory of take offs to process address space.\n");
		exit(-1);
	}
	if((list_land = (int*)shmat(lnd_id, NULL, 0)) < (int*)1){
		perror("Error attaching shared memory of landings to process address space\n");
		exit(-1);
	}

	#ifdef DEBUG
	printf("Creating named pipe.\n");
	#endif
	unlink("INPUT_PIPE");
	mkfifo("INPUT_PIPE", O_CREAT|O_EXCL|0666);

	#ifdef DEBUG
	printf("Opening file descriptor for pipe.\n");
	#endif
	if((fpipe = open("INPUT_PIPE", O_RDWR)) < 0){
		perror("Could not open file descriptor for pipe.\n");
		exit(-1);
	}

	#ifdef DEBUG
	printf("Opening file descriptor for log.txt.\n");
	#endif
	if(!(flog = fopen("log.txt", "a"))){
		perror("Could not open file descriptor for log.txt.\n");
		exit(-1);
	}
	c_time = time(NULL);
	f_time = gmtime(&c_time);
	fprintf(flog, "%2d:%2d:%2d PROGRAM STARTED\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec);

	#ifdef DEBUG
	printf("Creating message queue.\n");
	#endif
	/*if((msgq_id = msgget(IPC_PRIVATE, IPC_CREAT|0666) == -1)){ no idea why this doesn't work
		perror("Error creating message queue.\n"); supposedly return value is -1 if not successful
	}*/
	msgq_id = msgget(IPC_PRIVATE, IPC_CREAT|0666);

	#ifdef DEBUG
	printf("Creating control tower process.\n");
	#endif
	if((tower = fork()) == 0){
		tower_control();
		exit(0);
	}

	#ifdef DEBUG
	printf("Redirecting SIGINT.\n"); 
	#endif
	signal(SIGINT, sigint);		

	#ifdef DEBUG
	printf("Start time: %ld\n", time_s);
	#endif
	printf("Process is listening to pipe.\n");

	char buffer[MAX_BUFF];
	int n_read;
	char c;
	flight_landing lnd;
	flight_takeoff toff;
	


	pthread_t temp;

	//reading and verifying pipe's output
	while(1){
		pthread_mutex_lock(&valid_mutex);
		while(valid != 0){
			pthread_cond_wait(&valid_cond, &valid_mutex);
		}
		n_read = 0;
		read(fpipe, &c, 1);
		while(c != '\n'){
			buffer[n_read] = c;
			read(fpipe, &c, 1);
			n_read++;
		}
		//n_read = read(fpipe, buffer, MAX_BUFF);
		buffer[n_read] = '\0';
		if(sscanf(buffer, "ARRIVAL %s init: %d eta: %d fuel: %d", lnd.flight_id, &lnd.init, &lnd.eta, &lnd.fuel) == 4){
			if((lnd.init > (time(NULL) - time_s)) && lnd.fuel > lnd.eta){
				valid++;
				#ifdef DEBUG
				printf("\nTime since start: %ld\n", (time(NULL) - time_s));
				#endif
				lnd.time_s = time_s;
				c_time = time(NULL);
				f_time = gmtime(&c_time);
				printf("%2d:%2d:%2d NEW COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_lock(&log_mutex);
				fprintf(flog, "%2d:%2d:%2d NEW COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_unlock(&log_mutex);
				pthread_create(&temp, NULL, l_creation_timer, &lnd);
			}
			else{
				#ifdef DEBUG
				printf("\nTime since start: %ld\n", (time(NULL) - time_s));
				printf("Incompatible values\n");
				#endif
				c_time = time(NULL);
				f_time = gmtime(&c_time);
				printf("%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_lock(&log_mutex);
				fprintf(flog, "%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_unlock(&log_mutex);
			}
		}
		else if(sscanf(buffer, "DEPARTURE %s init: %d takeoff: %d", toff.flight_id, &toff.init, &toff.t_off) == 3){
			if((toff.init > (time(NULL) - time_s)) && toff.t_off > toff.init){
				#ifdef DEBUG
				printf("\nTime since start: %ld\n", (time(NULL) - time_s));
				#endif
				valid++;
				toff.time_s = time_s;
				c_time = time(NULL);
				f_time = gmtime(&c_time);
				printf("%2d:%2d:%2d NEW COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_lock(&log_mutex);
				fprintf(flog, "%2d:%2d:%2d NEW COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_unlock(&log_mutex);
				pthread_create(&temp, NULL, t_creation_timer, &toff);
			}
			else{
				#ifdef DEBUG
				printf("\nTime since start: %ld\n", (time(NULL) - time_s));
				printf("Incompatible values\n");
				#endif
				c_time = time(NULL);
				f_time = gmtime(&c_time);
				printf("%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_lock(&log_mutex);
				fprintf(flog, "%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
				pthread_mutex_unlock(&log_mutex);
			}
		}
		else{
			#ifdef DEBUG
			printf("\nTime since start: %ld\n", (time(NULL) - time_s));
			printf("Wrong formatting.\n");
			#endif
			c_time = time(NULL);
			f_time = gmtime(&c_time);
			printf("%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
			pthread_mutex_lock(&log_mutex);
			fprintf(flog, "%2d:%2d:%2d WRONG COMMAND => %s\n", f_time->tm_hour, f_time->tm_min, f_time->tm_sec, buffer);
			pthread_mutex_unlock(&log_mutex);
		}
		pthread_mutex_unlock(&valid_mutex);
	}
	
	return 0;
}


//Tower control process-----
void *t_msgqueue(void *t){
	takeoff_msg msg;	
	while(1){
		msgrcv(msgq_id, &msg, sizeof(msg) - sizeof(long), 10, 0);
		#ifdef DEBUG
		printf("MSG QUEUE RECEIVED:\ntakeoff: %d\n", msg.t_off);
		#endif
		//REMEMBER TO BUILD MESSAGE TO SEND WELL
		msgsnd(msgq_id, &msg_send, sizeof(msg_send) - sizeof(long), 0);
	}
}

void *l_msgqueue(void *t){
	landing_msg msg;
	while(1){
		msgrcv(msgq_id, &msg, sizeof(msg) - sizeof(long), 20, 0);
		#ifdef DEBUG
		printf("MSG QUEUE RECEIVED:\neta: %d\nfuel: %d\n", msg.eta, msg.fuel);
		#endif
		//REMEMBER TO BUILD MESSAGE TO SEND WELL
		msgsnd(msgq_id, &msg_send, sizeof(msg_send) - sizeof(long), 0);
	}
}

void tower_control(){
	pthread_t temp;
	//printf("please\n");
	int a = 0;//must send something in the 4th parameter, NULL not accepted
	pthread_create(&temp, NULL, t_msgqueue, &a);
	pthread_create(&temp, NULL, l_msgqueue, &a);
	pthread_exit(NULL);
}
//Tower control process-----



















 
